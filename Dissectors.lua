
local my_random_protocol = Proto("my_random", "My random Protocol")


local fields = {
    sequence_number = ProtoField.uint32("my_random.sequence_number", "Sequence Number"),
    payload_length = ProtoField.uint16("my_random.payload_length", "Payload Length"),
    payload_data = ProtoField.string("my_random.payload_data", "Payload Data")
}

my_random_protocol.fields = fields


function my_random_protocol.dissector(buffer, pinfo, tree)
    pinfo.cols.protocol:set("MyrandomProtocol")

    
    local subtree = tree:add(my_random_protocol, buffer(), "My random Protocol Data")

    
    local sequence_number = buffer(0, 4):uint()
    local payload_length = buffer(4, 2):uint()

    
    subtree:add(fields.sequence_number, buffer(0, 4)):append_text(" (Sequence Number: " .. sequence_number .. ")")
    subtree:add(fields.payload_length, buffer(4, 2)):append_text(" (Payload Length: " .. payload_length .. " bytes)")

    
    local header_length = 8
    local remaining_length = buffer:len() - header_length
    if remaining_length >= payload_length then
        
        local payload_data = buffer(header_length, payload_length):string()
        subtree:add(fields.payload_data, buffer(header_length, payload_length)):append_text(" (Payload Data: " .. payload_data .. ")")
    else
        
        subtree:add_expert_info(PI_MALFORMED, PI_ERROR, "Incomplete payload data")
    end
end


local tcp_port_table = DissectorTable.get("tcp.port")
tcp_port_table:add(443, my_random_protocol)

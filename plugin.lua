local f_tcp_window = Field.new("tcp.window_size")
local f_tcp_length = Field.new("tcp.len")
local f_tcpstream = Field.new("tcp.stream")
local f_irtt = Field.new("tcp.analysis.initial_rtt")
local f_syn = Field.new("tcp.flags.syn")
local f_ack = Field.new("tcp.flags.ack")

local sample_stream_protocol = Proto("sample_stream_protocol", "Stream Stat Extraction")

local F_tcp_win = ProtoField.int32("sample_stream_protocol.tcp_window_size", "Stats for Calculation")
local F_irtt = ProtoField.double("sample_stream_protocol.irtt", "Initial Roundtrip Time")
local stat_highirtt = ProtoField.double("sample_stream_protocol.highirtt", "Highest Roundtrip Time")
local F_packetstudy = ProtoField.string("sample_stream_protocol.study", "Packet Study")
sample_stream_protocol.fields = {F_tcp_win, F_irtt, stat_highirtt, F_packetstudy}

stream_data = {}
highest_irtt = 0

function sample_stream_protocol.dissector(tvb, packetinfo, tree)
    local tcp_len = f_tcp_length()
    local tcp_window = f_tcp_window().len
    local tcpstream = f_tcpstream().value
    local packet_study = "none"
    
    if tcp_len then
        tcp_len = f_tcp_length().value
        local irtt = f_irtt() and tonumber(tostring(f_irtt().value)) or 0

        local ack_flag = f_ack().value
        local syn_flag = f_syn().value
        
        if not packetinfo.visited then
            if not stream_data[tcpstream] then
                if not ack_flag or syn_flag then
                    if syn_flag then
                        if tcp_window then
                            packet_study = "SYN packet, supported"
                        else
                            packet_study = "SYN packet, no support"
                        end
                    end
                    stream_data[tcpstream] = true
                    highest_irtt = 0
                end
            end
        end
        
        if irtt > highest_irtt then
            highest_irtt = irtt
        end
        
        local subtree = tree:add(sample_stream_protocol, "Stream Extraction")
        subtree:add(F_tcp_win, tcp_window)
        subtree:add(F_irtt, irtt):set_generated()
        subtree:add(stat_highirtt, highest_irtt):set_generated()
        subtree:add(F_packetstudy, packet_study)
    end
end

register_postdissector(sample_stream_protocol)
